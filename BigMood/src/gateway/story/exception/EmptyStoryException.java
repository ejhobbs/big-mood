package gateway.story.exception;

public class EmptyStoryException extends StoryException {
    public EmptyStoryException() {
        super("Loaded story has no content!");
    }
}
