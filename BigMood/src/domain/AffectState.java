package domain;

/**
 * Point on the Russel '2d circumplex of emotion'
 */
public class AffectState {
    private int x,y;

    public AffectState(int x, int y){
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public String toString(){
        return String.format("(%d,%d)", this.x, this.y);
    }

}
