package gateway;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class FilesystemInterface {

    /**
     * Get list of lines from file with given encoding. For valid charsets see Charset.availableCharsets()
     * @param filename filename
     * @param encoding file encoding
     * @return all lines from file
     * @throws IOException
     */
    public List<String> getAllLines(String filename, String encoding) throws Exception {
        List<String> lines = new ArrayList<>();
        forEachLine(filename, encoding, (i, s) -> lines.add(s));
        return lines;
    }

    /**
     * Iterate over all lines in file, applying f to each
     * @param filename
     * @param encoding
     * @param f
     * @throws Exception
     */
    public void forEachLine(String filename, String encoding, FileLineConsumer f) throws Exception {
        BufferedReader lines = getBufferedReader(filename, encoding);
        int p = 0;
        String line = lines.readLine();
        while (line != null) {
            f.apply(p, line);
            p++;
            line = lines.readLine();
        }
    }

    private BufferedReader getBufferedReader(String filename, String encoding) throws IOException {
        Path p = Paths.get(filename);
        Charset c = Charset.forName(encoding);
        return Files.newBufferedReader(p, c);
    }
}
