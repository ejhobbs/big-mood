package gateway.story;

import domain.Sentence;
import gateway.story.exception.ClassificationException;

public interface SentenceClassifier {
    void classify(Sentence s) throws ClassificationException;
}