package gateway.dictionary;

import domain.AffectDictionary;
import gateway.dictionary.exception.DictionaryException;

public interface DictionaryLoader {
    AffectDictionary populate(String s) throws DictionaryException;
    AffectDictionary populate(String s1, String s2) throws DictionaryException;
}
