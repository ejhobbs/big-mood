package gateway.story.exception;

public class MissingStoryException extends StoryException {
    public MissingStoryException(String s) {
        super(String.format("Could not load story: %s", s));
    }
}
