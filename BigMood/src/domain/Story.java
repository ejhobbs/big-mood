package domain;

import java.util.ArrayList;
import java.util.List;

public class Story {
    private List<Sentence> sentences = new ArrayList<>();
    private List<CastMember> cast = new ArrayList<>();

    public List<Sentence> getSentences() {
        return sentences;
    }

    public List<CastMember> getCast() {
        return cast;
    }

    public void addSentence(Sentence s) {
        sentences.add(s);
    }

    public void addCastMember(CastMember c) {
        cast.add(c);
    }

    public boolean hasContent() {
        return !(sentences.isEmpty() || cast.isEmpty());
    }
}
