package domain;

import java.util.Arrays;
import java.util.List;

public class Sentence {
    private final List<String> raw;
    private CastMember target;
    private AffectState affect;

    public Sentence(String raw) {
        this.raw = Arrays.asList(raw.split(" "));
    }

    public List<String> getRaw() {
        return raw;
    }

    public CastMember getTarget() {
        return target;
    }

    public AffectState getAffect() {
        return affect;
    }

    public void setTarget(CastMember target) {
        this.target = target;
    }

    public void setAffect(AffectState affect) {
        this.affect = affect;
    }
}
