package usecase;

import domain.AffectLUT;
import domain.CastMember;
import domain.Sentence;
import domain.Story;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

public class GenerateGradientsForStory {
    private final int height;
    private final int width;

    public GenerateGradientsForStory(int height, int width) {
        this.height = height;
        this.width = width;
    }

    public Map<String, LinearGradientPaint> generate(AffectLUT l, Story s) {
        Map<CastMember, Pairs> timelines = new HashMap<>();
        float inc = (float)1/s.getSentences().size();
        float idx = 0;
        for(Sentence sentence: s.getSentences()) {
            CastMember subject = sentence.getTarget();
            if(subject != null) {
                Color c = l.getColour(sentence.getAffect());
                if (timelines.containsKey(subject)) {
                    timelines.get(subject).add(idx, c);
                } else {
                    timelines.put(subject, new Pairs(idx, c));
                }
            }
            idx += inc;
        }

        Map<String, LinearGradientPaint> gradients = new HashMap<>();
        for(CastMember cm: timelines.keySet()) {
            Pairs p = timelines.get(cm);
            LinearGradientPaint lp = new LinearGradientPaint(0,0,width,height
            , p.getOffsets(), p.getColours());
            gradients.put(cm.getName(), lp);
        }
        return gradients;
    }

    private class Pairs {
        Pairs(float k, Color v) {
            this.k = new ArrayList<>();
            this.v = new ArrayList<>();
            this.k.add(k);
            this.v.add(v);
        }
        void add(float k, Color v) {
            this.k.add(k); this.v.add(v);
        }
        float[] getOffsets() {
            float[] offs = new float[k.size()];
            for (int i = 0; i < k.size(); i++) {
                offs[i] = k.get(i);
            }
            return offs;
        }
        Color[] getColours() {
            Color[] colours = new Color[v.size()];
            for (int i = 0; i < v.size(); i++) {
                colours[i] = v.get(i);
            }
            return colours;
        }

        List<Float> k; List<Color> v;
    }
}
