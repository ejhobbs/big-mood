package gateway.dictionary.exception;

public class MissingDictionaryException extends DictionaryException {
    public MissingDictionaryException(String m) {
        super(String.format("Could not read file %s, does it exist?", m));
    }
}
