package domain;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;

public class ImageAffectLUT implements AffectLUT {
    private final BufferedImage img;
    private final float scale;
    private final int res;

    public ImageAffectLUT(BufferedImage img, int resolution) {
        this.img = img;
        this.res = resolution;
        this.scale = ((float)img.getHeight()/2)/(float) resolution;
    }

    @Override
    public Color getColour(AffectState as) {
        int x = scale(as.getX());
        int y = scale(as.getY());
        return new Color(img.getRGB(x, y));
    }

    @Override
    public ColorModel getColourModel() {
        return img.getColorModel();
    }

    private int scale(int i) {
        return (int) (scale*(i+res));
    }

}
