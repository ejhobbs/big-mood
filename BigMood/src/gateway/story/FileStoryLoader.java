package gateway.story;

import domain.CastMember;
import domain.Sentence;
import domain.Story;
import gateway.FilesystemInterface;
import gateway.story.exception.MissingCastException;
import gateway.story.exception.MissingStoryException;
import gateway.story.exception.StoryException;

public class FileStoryLoader implements StoryLoader {
    private final String castMarker;
    private final FilesystemInterface fs = new FilesystemInterface();

    public FileStoryLoader(String castMarker) {
        this.castMarker = castMarker;
    }

    public FileStoryLoader() {
        this("CAST:");
    }

    @Override
    public Story get(String s) throws StoryException {
        return get(s, "UTF-8");
    }

    @Override
    public Story get(String filename, String charset) throws StoryException {
        Story s = new Story();
        try {
            fs.forEachLine(filename, charset, (pos, line) -> {
                if(line.startsWith(castMarker)) {
                    processCast(s, line.replace(castMarker, "").trim());
                } else {
                    s.addSentence(new Sentence(line.trim()));
                }
            });
        }
        catch (StoryException e) { throw e; }
        catch (Exception e) { throw new MissingStoryException(filename); }
        return s;
    }

    private void processCast(Story s, String cast) throws MissingCastException {
        String[] members = cast.split(",");
        if(members.length <= 0) {
            throw new MissingCastException();
        }
        for (String member: members) {
            s.addCastMember(new CastMember(member.trim()));
        }
    }
}
