package gateway.drawing;

public class LUTNotFoundException extends Exception {
    public LUTNotFoundException(String message) {
        super(message);
    }
}
