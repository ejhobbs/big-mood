package gateway.drawing;

import domain.GradientHolder;

import javax.swing.*;
import java.awt.*;
import java.util.Map;

public class Frame {
    private JFrame frame;
    private int width;
    private int height;

    public Frame(String name, int width, int height) {
        this.frame = new JFrame(name);
        this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.width = width;
        this.height = height;

    }
    public void display(Map<String, LinearGradientPaint> gradients) {
        JPanel panel = new JPanel(new GridLayout(gradients.size(), 1));
        for (LinearGradientPaint paint: gradients.values()) {
            panel.add(new GradientHolder(paint));
        }
        frame.add(panel);
        frame.pack();
        frame.setVisible(true);
    }
}
