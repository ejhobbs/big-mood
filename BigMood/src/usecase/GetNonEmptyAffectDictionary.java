package usecase;

import domain.AffectDictionary;
import gateway.dictionary.DictionaryLoader;
import gateway.dictionary.exception.DictionaryException;
import gateway.dictionary.exception.EmptyDictionaryException;

public class GetNonEmptyAffectDictionary {
    private final DictionaryLoader dictionaryLoader;

    public GetNonEmptyAffectDictionary(DictionaryLoader dictionaryLoader) {
        this.dictionaryLoader = dictionaryLoader;
    }

    public AffectDictionary exec(String filename) throws DictionaryException {
        AffectDictionary dict = dictionaryLoader.populate(filename);
        if (dict.isEmpty()){
            throw new EmptyDictionaryException();
        }
        return dict;
    }
}
