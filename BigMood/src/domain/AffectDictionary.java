package domain;

import java.util.HashMap;
import java.util.Map;

public class AffectDictionary {
    Map<String, AffectState> dict = new HashMap<>();

    public AffectState lookupWord(String s) throws WordNotFoundException {
        if (containsWord(s)) {
            return dict.get(s);
        }
        throw new WordNotFoundException(String.format("Could not find word \"%s\"", s));
    }

    public boolean containsWord(String s) {
        return dict.containsKey(s);
    }

    public boolean isEmpty(){
        return dict.isEmpty();
    }

    public void addWord(String word, AffectState s) {
        dict.put(word, s);
    }
}
