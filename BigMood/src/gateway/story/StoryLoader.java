package gateway.story;

import domain.Story;
import gateway.story.exception.StoryException;

public interface StoryLoader {
    Story get(String s) throws StoryException;
    Story get(String s1, String s2) throws StoryException;
}