package gateway.story.exception;

import domain.AffectState;
import domain.CastMember;

public class ClassificationException extends StoryException {
    public ClassificationException(String s, AffectState a, CastMember t) {
        super(String.format("Unable to classify sentence: %s\n" +
                "Affect: %s\n" +
                "Target: %s\n" +
                "Make sure that your dictionary is complete!", s, a, t));
    }
}
