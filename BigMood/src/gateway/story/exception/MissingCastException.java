package gateway.story.exception;

public class MissingCastException extends StoryException {
    public MissingCastException() {
        super("Story has no cast members!");
    }
}
