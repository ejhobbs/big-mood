package gateway;

public interface FileLineConsumer {
    void apply(int i, String s) throws Exception;
}
