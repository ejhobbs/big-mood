package gateway.drawing;

import domain.AffectLUT;
import domain.ImageAffectLUT;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class FileLUTLoader implements LUTLoader {
    @Override
    public AffectLUT load(String s, int resolution) throws LUTNotFoundException {
        File imgFile = new File(s);
        try {
            BufferedImage img = ImageIO.read(imgFile);
            return new ImageAffectLUT(img, resolution);
        } catch (IOException e) {
            throw new LUTNotFoundException(e.getMessage());
        }
    }
}
