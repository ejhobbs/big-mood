package gateway.dictionary.exception;

public class EmptyDictionaryException extends DictionaryException {
    public EmptyDictionaryException() {
        super("Empty dictionary provided!");
    }
}
