package gateway.story.exception;

public class StoryException extends Exception {
    public StoryException(String m) {
        super(m);
    }
}
