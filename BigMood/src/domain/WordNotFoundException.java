package domain;

public class WordNotFoundException extends Exception {
    public WordNotFoundException(String m) {
        super(m);
    }
}
