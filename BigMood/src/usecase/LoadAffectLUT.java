package usecase;

import domain.AffectLUT;
import gateway.drawing.LUTLoader;
import gateway.drawing.LUTNotFoundException;

public class LoadAffectLUT {
    private final LUTLoader loader;

    public LoadAffectLUT(LUTLoader loader) {
        this.loader = loader;
    }

    public AffectLUT load(String source, int resolution) throws LUTNotFoundException {
        return loader.load(source, resolution);
    }
}
