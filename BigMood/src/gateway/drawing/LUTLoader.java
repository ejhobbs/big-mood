package gateway.drawing;

import domain.AffectLUT;


public interface LUTLoader {
    AffectLUT load(String s, int resolution) throws LUTNotFoundException;
}
