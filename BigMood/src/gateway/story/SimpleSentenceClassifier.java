package gateway.story;
import domain.*;
import gateway.story.exception.ClassificationException;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class SimpleSentenceClassifier implements SentenceClassifier {
    private final List<CastMember> cast;
    private final AffectDictionary dict;
    private final List<String> verbs;

    public SimpleSentenceClassifier(List<CastMember> cast, AffectDictionary dict) {
        this.cast = cast;
        this.dict = dict;
        verbs = Arrays.asList("is","was");
    }

    @Override
    public void classify(Sentence s) throws ClassificationException {
        List<String> words = s.getRaw();
        boolean classifiable = false;

        Iterator<String> wordIt = words.iterator();
        while(wordIt.hasNext() && !classifiable) {
            classifiable = verbs.contains(wordIt.next());
        }
        //only try to classify a sentence if it contains some form of 'to be'
        if (classifiable) {
            CastMember target = null;
            AffectState affect = null;
            for (CastMember member : cast) {
                String m = member.getName();
                if (words.contains(m)) {
                    target = member;
                }
            }
            for (String word : words) {
                if (dict.containsWord(word)) {
                    try {
                        affect = dict.lookupWord(word);
                    } catch (WordNotFoundException ignored) {
                        // can't happen
                    }
                }
            }
            if (affect == null || target == null) {
                throw new ClassificationException(String.join(" ", words), affect, target);
            }
            s.setAffect(affect);
            s.setTarget(target);
        }
    }
}
