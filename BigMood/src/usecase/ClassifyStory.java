package usecase;

import domain.Sentence;
import domain.Story;
import gateway.story.SentenceClassifier;
import gateway.story.exception.ClassificationException;

import java.util.List;

public class ClassifyStory {
    private final SentenceClassifier sc;

    public ClassifyStory(SentenceClassifier sc) {
        this.sc = sc;
    }

    public void classify(Story s) throws ClassificationException {
        List<Sentence> ss = s.getSentences();
        for(Sentence sentence: ss) {
            sc.classify(sentence);
        }
    }
}
