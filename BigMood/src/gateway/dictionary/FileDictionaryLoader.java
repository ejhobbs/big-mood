package gateway.dictionary;

import domain.AffectDictionary;
import domain.AffectState;
import gateway.FilesystemInterface;
import gateway.dictionary.exception.DictionaryException;
import gateway.dictionary.exception.MalformedDictionaryException;
import gateway.dictionary.exception.MissingDictionaryException;

public class FileDictionaryLoader implements DictionaryLoader {
    private int resMax, resMin;
    private FilesystemInterface fs = new FilesystemInterface();
    /**
     * Create new File Dictionary
     * @param res axis resolution
     */
    public FileDictionaryLoader(int res) {
        this.resMax = res;
        this.resMin = -res;
    }
    /**
     * Clears dictionary and populates with entries from file
     * @param src source file
     * @throws DictionaryException
     */
    @Override
    public AffectDictionary populate(String src) throws DictionaryException {
        return populate(src, "UTF-8");
    }

    /**
     * Clears dictionary and populates with entries from file
     * @param src source file
     * @param charset file encoding
     * @throws DictionaryException
     */
    @Override
    public AffectDictionary populate(String src, String charset) throws DictionaryException {
        AffectDictionary d = new AffectDictionary();
        try {
            fs.forEachLine(src, charset, (pos, line) -> {
                if (!(line.isEmpty() || line.startsWith("%"))) {
                    addLineToDictionary(d, line);
                }
            }); }
        catch (DictionaryException e) { throw e; } //forward dictionary exceptions on
        catch (Exception e) { throw new MissingDictionaryException(src); }

        return d;
    }

    private void addLineToDictionary(AffectDictionary dict, String line) throws DictionaryException {
        String[] parts = line.split("\t");
        if (parts.length < 3) {
            throw new MalformedDictionaryException(line, "Missing values");
        }
        try {
            int x = Integer.parseInt(parts[1]);
            int y = Integer.parseInt(parts[2]);

            if (x < resMin || x > resMax || y < resMin || y > resMax) {
                throw new MalformedDictionaryException(line, "Values provided outside expected range");
            }
            AffectState s = new AffectState(x,y);
            dict.addWord(parts[0], s);
        } catch (NumberFormatException e) {
            throw new MalformedDictionaryException(line, "Non-numerical values provided");
        }
    }
}
