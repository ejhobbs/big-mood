package gateway.dictionary.exception;

public class MalformedDictionaryException extends DictionaryException{
    public MalformedDictionaryException(String line, String m) {
        super(String.format("Malformed dictionary line: %s\n%s", line, m));
    }
}
