import domain.AffectDictionary;
import domain.AffectLUT;
import domain.Story;

import gateway.dictionary.exception.DictionaryException;
import gateway.dictionary.FileDictionaryLoader;
import gateway.drawing.FileLUTLoader;
import gateway.drawing.Frame;
import gateway.drawing.LUTNotFoundException;
import gateway.story.FileStoryLoader;
import gateway.story.SentenceClassifier;
import gateway.story.SimpleSentenceClassifier;
import gateway.story.exception.StoryException;

import usecase.*;

import javax.swing.*;
import java.awt.*;
import java.util.Map;

/**
 * Entry point for Big Mood java project
 * @author eh362
 * 
 * Expects four arguments (in order):
 * Dictionary Location - location of dictionary showing affects of words
 * Dictionary resolution - int, maximum value of affects (symmetrical, ie 50 would mean words can have affect from -50,-50 to 50,50
 * Story Location - Location of story. For formatting see example stories.
 * Affect LUT location - image mapping from points on affect circumplex to colors. Size does not matter as values from dictionary will
 * be scaled automatically with regard to image size (although at least matching the dictionary resolution would be wise for best results)
 *
 */
public class Main {
    public static void main(String[] args) throws DictionaryException, StoryException, LUTNotFoundException {

        int dictionaryResolution = Integer.parseInt(args[1]);
        AffectDictionary dict = new GetNonEmptyAffectDictionary(new FileDictionaryLoader(dictionaryResolution))
                    .exec(args[0]);
        Story story = new GetUnprocessedStory(new FileStoryLoader()).exec(args[2]);
        SentenceClassifier sc = new SimpleSentenceClassifier(story.getCast(), dict);
        ClassifyStory classify = new ClassifyStory(sc);
        classify.classify(story);

        AffectLUT lut = new LoadAffectLUT(new FileLUTLoader())
                .load(args[3], dictionaryResolution);

        GenerateGradientsForStory gradientGen = new GenerateGradientsForStory(10,500);
        Map<String, LinearGradientPaint> gradients = gradientGen.generate(lut, story);

        Frame f = new Frame("Big Mood", 500,100);
        SwingUtilities.invokeLater(() -> f.display(gradients));
    }
}
