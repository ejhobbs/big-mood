package domain;

import java.awt.*;
import java.awt.image.ColorModel;

public interface AffectLUT {
    Color getColour(AffectState as);
    ColorModel getColourModel();
}
