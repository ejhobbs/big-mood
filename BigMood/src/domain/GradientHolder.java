package domain;

import javax.swing.*;
import java.awt.*;

public class GradientHolder extends JPanel {
    private final LinearGradientPaint paint;

    public GradientHolder(LinearGradientPaint paint) {
        super();
        this.paint = paint;
    }
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        Graphics2D g2d = (Graphics2D) g;
        g2d.setPaint(this.paint);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        g2d.fillRect(0,0, screenSize.width, screenSize.height);
    }
}
