package usecase;

import domain.Story;
import gateway.story.StoryLoader;
import gateway.story.exception.EmptyStoryException;
import gateway.story.exception.StoryException;

public class GetUnprocessedStory {
    private final StoryLoader loader;

    public GetUnprocessedStory(StoryLoader loader) {
        this.loader = loader;
    }

    public Story exec(String filename) throws StoryException {
        Story s = loader.get(filename);
        if(!s.hasContent()) {
            throw new EmptyStoryException();
        }
        return s;
    }
}
